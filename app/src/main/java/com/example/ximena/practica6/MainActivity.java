package com.example.ximena.practica6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button sensor,vibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sensor =findViewById(R.id.btnSensorr);
        vibrar =findViewById(R.id.btnVibrarr);

        sensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(
                        MainActivity.this,ActidadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        vibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(
                        MainActivity.this,ActividadVibracion.class);
                startActivity(intent);
            }
        });

    }
}
